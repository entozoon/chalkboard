<!DOCTYPE html>
<html lang="en">
	<head>
	 
	<title>Chalkboard</title>

	<!-- Prevents touch devices from scrolling or zooming -->
	<meta name="viewport" content="user-scalable=no, initial-scale=0.5, minimum-scale=0.5, maximum-scale=0.5"/>

	<!-- allow iDevices to save the site as a pseudo app, and prevent caching -->	
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />

	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	<link rel="apple-touch-icon" sizes="57x57" href="images/icon-small.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="images/icon-medium.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="images/icon-large.png" />

	<link rel="icon" href="images/favicon.ico" type="image/x-icon">

	<?php
		foreach (glob("css/*.css") as $filename)
			echo "<link rel='stylesheet' type='text/css' href='$filename'>\n";

		foreach (glob("js/*.js") as $filename)
			if ($filename != 'js/UnionDraw.js')
				echo "<script type='text/javascript' src='$filename'></script>\n";
	?>
	
</head>

<body>

	<div id='chalkboard'>	
		<div id="controls">
			Size:
			<select id="thickness" class="fixed">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="10" selected='selected'>10</option>
				<option value="20">20</option>
			</select>

		</div>

		<?php
		  // We're supposed to start with these operation programs first. 
		 // That's major boring shit. 
		// Let's do something a little more fun.
		?>
		<div id='tools'>
		<div id='colour'>#4959AB</div>
			<div id="colourpicker"></div>
		</div>
		
		<!--The canvas where drawings will be displayed-->
		<!--
		<canvas id="canvas" width="800" height="400"></canvas>
		-->
		<div id="board"><canvas id="canvas"></canvas></div>
	 
		<!--A status text field, for displaying connection information
		<div id="status"></div>-->
	</div>
	<div id='dwa'>
	</div>
</body>
</html>
