var mouse_down = false;
var begin_path = false;
var canvas, ctx, canvasPositionX, canvasPositionY, tmp_canvas, tmp_ctx, last_mouse, mouse, ppts, colour;
var iOS = false;

$(document).ready(function() {
	if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) iOS = true;

	// Stop the page being scrolled on touchscreen devices
	$('body').bind('touchmove', function() {
		event.preventDefault();
	});

	// Set up the colour picker
	$('#colourpicker').farbtastic(function(colour) {
		//c(colour);
		$('#colourpicker').css('background', colour);
		$('#colour').html(colour);
	});
	colourpicker = $.farbtastic('#colourpicker');
	colourpicker.setColor('#4959AB');



	// Set up the canvas	
	canvas = document.querySelector('#canvas');
	ctx = canvas.getContext('2d');
	
	var board = document.querySelector('#board');
	var board_style = getComputedStyle(board);
	canvas.width = parseInt(board_style.getPropertyValue('width'));
	canvas.height = parseInt(board_style.getPropertyValue('height'));
	
	
	// Create a tmp canvas
	tmp_canvas = document.createElement('canvas');
	tmp_ctx = tmp_canvas.getContext('2d');
	tmp_canvas.id = 'tmp_canvas';
	tmp_canvas.width = canvas.width;
	tmp_canvas.height = canvas.height;
	
	//board.appendChild(tmp_canvas);
	$('#canvas').after(tmp_canvas);

	mouse = {x: 0, y: 0};
	last_mouse = {x: 0, y: 0};
	
	// Pencil Points
	ppts = [];
	
	tmp_ctx.lineCap = 'round'; // square or butt available, but they're weird
	tmp_ctx.lineJoin = 'round';




 	tmp_canvas.addEventListener("mousedown", mouseDown, false);
	tmp_canvas.addEventListener("touchstart", mouseDown, false);


	tmp_canvas.addEventListener("mousemove", mouseMove, false);
	tmp_canvas.addEventListener("touchmove", mouseMove, false);

	//tmp_canvas.addEventListener("mouseout", mouseUp, false);
	tmp_canvas.addEventListener("touchend", mouseUp, false);
	document.body.addEventListener("mouseup", mouseUp, false);
	document.body.addEventListener("touchcancel", mouseUp, false);


	// stop the browser thinking that dragging the mouse is text selection (cursor change)
	tmp_canvas.onselectstart = function () { return false; } // ie
	tmp_canvas.onmousedown = function () { return false; } // mozilla

	// figure out the canvas position for offsets
	canvasPositionX = $('#canvas').offset().left;
	canvasPositionY = $('#canvas').offset().top;

});


function mouseMove(e) {
	getPosition(e);
}

function mouseDown(e) {
	//colour = hexToRgb($('#colour').html());
	colour = $('#colour').html(); // fine as hex all along, sigh.
	//tmp_ctx.strokeStyle = "rgb(0, 0, 255)";
	//tmp_ctx.strokeStyle = "rgb(" + colour[0] + ", " + colour[1] + ", " + colour[2] + ")";
	tmp_ctx.strokeStyle = colour;
	tmp_ctx.lineWidth = $('#thickness').val();

	tmp_canvas.addEventListener('touchmove', onPaint, false);
	tmp_canvas.addEventListener('mousemove', onPaint, false);
	
	getPosition(e);
	
	ppts.push({x: mouse.x, y: mouse.y});
	
	onPaint();
}

function mouseUp(e) {
	tmp_canvas.removeEventListener('touchmove', onPaint, false);
	tmp_canvas.removeEventListener('mousemove', onPaint, false);
	
	// Writing down to real canvas now
	ctx.drawImage(tmp_canvas, 0, 0);
	// Clearing tmp canvas
	tmp_ctx.clearRect(0, 0, tmp_canvas.width, tmp_canvas.height);

	// Emptying up Pencil Points
	ppts = [];
}

function getPosition(e) { // could split into separate functions for efficiency
	//mouse.x = typeof e.offsetX !== 'undefined' ? e.offsetX : e.layerX;
	//mouse.y = typeof e.offsetY !== 'undefined' ? e.offsetY : e.layerY;

	// touchscreen devices (uses the first touch, not multi touch) except iOS which is happy with normal layer
	if (e.touches && !iOS) {
		// offset to be properly on the canvas
		mouse.x = e.touches[0].pageX - canvasPositionX;
		mouse.y = e.touches[0].pageY - canvasPositionY;
	}
	
	// web browsers or iOS
	else if (e.layerX) {
		mouse.x = e.layerX;
		mouse.y = e.layerY;
	}
	else {
		alert("Can't understand your device, email us!");
	}
}

function onPaint() {

	// Saving all the points in an array
	ppts.push({x: mouse.x, y: mouse.y});

	if (ppts.length < 3) {
		var b = ppts[0];
		tmp_ctx.beginPath();
		//ctx.moveTo(b.x, b.y);
		//ctx.lineTo(b.x+50, b.y+50);

		tmp_ctx.arc(b.x, b.y, tmp_ctx.lineWidth / 2, 0, Math.PI * 2, !0);

		tmp_ctx.fillStyle = colour;
		tmp_ctx.fill();
		tmp_ctx.closePath();
		
		return;
	}
	// Tmp canvas is always cleared up before drawing.
	tmp_ctx.clearRect(0, 0, tmp_canvas.width, tmp_canvas.height);
	
	tmp_ctx.beginPath();
	tmp_ctx.moveTo(ppts[0].x, ppts[0].y);
	
	for (var i = 1; i < ppts.length - 2; i++) {
		var c = (ppts[i].x + ppts[i + 1].x) / 2;
		var d = (ppts[i].y + ppts[i + 1].y) / 2;
		
		tmp_ctx.quadraticCurveTo(ppts[i].x, ppts[i].y, c, d);
	}
	
	// For the last 2 points
	tmp_ctx.quadraticCurveTo(
		ppts[i].x,
		ppts[i].y,
		ppts[i + 1].x,
		ppts[i + 1].y
	);
	tmp_ctx.stroke();
	
}








function c(dwa) { console.log(dwa); }


function cutHex(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h}
function hexToRgb(h) {
	var r = parseInt((cutHex(h)).substring(0,2),16),
		g = parseInt((cutHex(h)).substring(2,4),16),
		b = parseInt((cutHex(h)).substring(4,6),16)
	return [r, g, b];
}
